#!/usr/bin/env bash

# Usage:
    # You MUST run this as root!

	# To download:  $curl -O https://gitlab.com/tsalex/deployment-scripts/raw/master/centos7_lamp_php7ius.sh
	# To run remotely:  bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/centos7_lamp_php7ius.sh)

	# To run without user prompts...
	# $(curl -sSL https://gitlab.com/tsalex/deployment-scripts/raw/master/centos7_lamp_php7ius.sh | bash)



# Install Software Repositories

    #Add IUS Repo
    bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/components_centos7/centos7_ius_addrepo.sh)  

    #Add MariaDB official Repo
    bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/components_centos7/centos7_mariadb_addrepo_official.sh)


# Install Apache and php

    # Install apache
    bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/components_centos7/centos7_apache_install.sh)

    # Install php7 from ius
    bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/components_centos7/centos7_apache_php_7_mod-php_ius_install.sh)

    # restart apache
    sudo systemctl restart httpd.service



# INSTALL MARIADB
    bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/components_centos7/centos7_mariadb_install.sh)


# Open FIREWALL PORTS for Apache & MariaDB

    #Open http and https ports
    bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/components_centos7/centos7_firewall_config_webserver.sh)


# CERTBOT SSL
    # read -p "Would you like to install CertBot SSL [y/n]? " CERTBOTINSTALL

    # if [[ "$CERTBOTINSTALL" =~ ^([yY][eE][sS]|[yY])+$ ]]
    # then
    #     bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/centos7_lemp_certbotssl.sh)
    # fi


# Finish off with 

    echo 
    echo 
    echo ... web server components all DONE!
    echo _________________________________________
    echo 
    echo remember to: 
    echo  == 1. run mysql_secure_installation  
    echo  == 2. check if webserver and php work:  browse to local address and test.
    echo  == 3. reboot and test
    echo 
    # echo  .. NOTE: 
    # echo  ...... to install certbot, run this script:
    # echo  ...... bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/centos7_lemp_certbotssl.sh)
    echo --------------------------------------------------------------
    echo 