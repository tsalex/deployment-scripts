#!/bin/bash

# Usage:
# bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/centos7_initialize_webserver.sh)

#  Notes: https://gist.github.com/n0ts/146c65b95a80b0a90934


# Hardening for CENTOS
bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/centos7_hardened.sh)
# bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/centos7_hardened_disableRootSSH.sh)
echo
echo =========== HARDENING DONE =============
echo

# Install Web Server Components -- NGINX, MariaDB, php7
bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/centos7_lemp_php7ius.sh)

	# PS3="Which server would you like to install? "
	# select option in Nginx_php7_php-fpm_ius Apache_php7_php-mod_ius quit
	# do
	#     case $option in
	#         Nginx_php7_php-fpm_ius)
	#             echo "installing nginx...."
	# 			bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/centos7_lemp_php7ius.sh)
	# 			echo "....done"
	#             break ;;
	#         Apache_php7_php-mod_ius)
	# 			echo "installing nginx...."
	# 			bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/centos7_lamp_php7ius.sh)
	# 			echo "....done"
	#             break ;;
	#         quit)
	#             break;;
	#      esac
	# done

	echo "next command"



echo
echo =========== WEBSERVER DONE =============
echo
echo For additional hardening, you can disable ROOT SSH Access.
echo There is a script for that at:
echo  https://gitlab.com/tsalex/deployment-scripts/raw/master/centos7_hardened_disableRootSSH.sh
echo 
echo --------------------------------------------------------------

echo You should reboot now.
