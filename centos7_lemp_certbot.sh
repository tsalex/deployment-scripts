#!/bin/bash

# install certbot and run setup
    echo === Installing Certbot SSL...
    yum install -y python2-certbot-nginx > /dev/null
    if [ $? != 0 ]; then exit 1; fi
    sudo certbot --nginx
    echo ......Certbot SSL Installed

# test certbot renewal
    echo === Testing Certbot SSL renewal...
    sudo certbot renew --dry-run
    if [ $? != 0 ]; then exit 1; fi
    echo ......Certbot test DONE


# add certbot to crontab
    echo === Adding Certbot cronjob...
	#write out current crontab
	crontab -l > ~/crontab.orig  > /dev/null
	crontab -l > crontab.new  > /dev/null
	#echo new cron into cron file
	0 3,22 * * * python -c 'import random; import time; time.sleep(random.random() * 3600)' && certbot renew
	#install new cron file
	crontab crontab.new
	rm crontab.new
    echo ......Certbot cron DONE

# Secure SSL and by Updating Diffie-Hellman Parameters
    # echo === Updating SSL parameters ...
	# sudo openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
	# ssl_dhparam /etc/ssl/certs/dhparam.pem;

echo  ======= CERTBOT IS NOW SETUP. Check your nginx configs to confirm.
# echo  ======= IMPORTANT: