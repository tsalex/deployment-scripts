#!/bin/bash

# Usage:
	# To run directly:   bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/centos7_hardened_disableRootSSH.sh)
	# To download:  $curl -O https://gitlab.com/tsalex/deployment-scripts/raw/master/centos7_hardened_disableRootSSH.sh
        

disableRootOverSSH(){
	sed -i -e "s/PermitRootLogin yes/PermitRootLogin no/" /etc/ssh/sshd_config
	sed -i -e "s/#PermitRootLogin no/PermitRootLogin no/" /etc/ssh/sshd_config
}

disablePasswordsOverSSH(){
	sed -i -e "s/PasswordAuthentication yes/PasswordAuthentication no/" /etc/ssh/sshd_config
	sed -i -e "s/#PasswordAuthentication no/PasswordAuthentication no/" /etc/ssh/sshd_config
}

restartSSH(){
	echo Restarting sshd...
	systemctl restart sshd
	echo ...done
}


# Run script by asking first
echo Disabling passwords and root login over ssh...
echo "WARNING: If you wish to login, you need to have another user account set up!"

read -r -p "Do you want to continue? [y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
then
    disableRootOverSSH
    disablePasswordsOverSSH
    restartSSH
else
    exit 1
fi
