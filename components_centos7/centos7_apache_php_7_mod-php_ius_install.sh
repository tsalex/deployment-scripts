#!/usr/bin/env bash

# Install php7 on nginx (php-fpm)
echo Installing php7 from IUS...
    sudo yum install -y mod_php70u php70u-cli php70u-common php70u-bcmath php70u-devel php70u-gd php70u-json php70u-ldap php70u-mbstring php70u-mcrypt php70u-mysqlnd php70u-fpm-nginx php70u-opcache php70u-pdo php70u-pear php70u-process php70u-tidy php70u-xml > /dev/null
    if [ $? != 0 ]; then exit 1; fi