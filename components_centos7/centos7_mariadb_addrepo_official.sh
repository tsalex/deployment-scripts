#!/usr/bin/env bash

# ====== Define Function =======

	# Add MARIADB OFFICIAL REPO
addRepo_MariaDB(){       
    echo Installing MariaDB official repositories...
    cat > /etc/yum.repos.d/MariaDB.repo <<EOF
# http://downloads.mariadb.org/mariadb/repositories/
[mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/10.1/centos7-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1
EOF
    echo ...MariaDB repo added
    
}


# ====== Run =======

	# ADD REPO
	addRepo_MariaDB