#!/usr/bin/env bash

# to run from bash:   
#   bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/components_centos7/centos7_apache_php_7-2_mod-php_ius_install.sh)

# Install php7.2 (mod_php for apache & php-fpm for nginx)
echo "Installing php7.2 from IUS..."
    sudo yum install -y mod_php72u php72u-cli php72u-common php72u-bcmath php72u-devel php72u-gd php72u-imap php72u-json php72u-ldap php72u-mbstring php72u-mysqlnd php72u-fpm-nginx php72u-opcache php72u-pdo php72u-pear php72u-process php72u-sodium php72u-tidy php72u-xml > /dev/null
    if [ $? != 0 ]; then exit 1; fi