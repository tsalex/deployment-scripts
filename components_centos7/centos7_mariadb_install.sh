#!/usr/bin/env bash


# INSTALL MARIADB
    echo Installing MariaDB...
    yum install -y mariadb-server > /dev/null
    systemctl enable mariadb > /dev/null
    systemctl start mariadb > /dev/null
    if [ $? != 0 ]; then exit 1; fi
    echo ... MariaDB DONE
    echo Remember to run mysql_secure_installation!
