#!/usr/bin/env bash

# ====== Define Function =======

# Open firewall for http and https
setupFirewall-webserver(){
    echo Enabling http and https in firewalld...
    firewall-cmd --permanent --add-service=http > /dev/null
    firewall-cmd --permanent --add-service=https > /dev/null
    echo ... reloading firewalld...
    firewall-cmd --reload > /dev/null
    if [ $? != 0 ]; then exit 1; fi
    echo ...Firewall DONE
}


# ====== Run =======


    # if statement checks if this is an ec2 instance (firewall is not managed locally for ec2)
    
    if [ -f /sys/hypervisor/uuid ] && [ `head -c 3 /sys/hypervisor/uuid` == ec2 ]; then
        
        # - it's an ec2 instance; do nothing - #
        echo 'this is an ec2 instance... no firewall changes to be made locally. Please check your EC2 security groups.'

    else

		# Setup Firewall
		setupFirewall-webserver

    fi	