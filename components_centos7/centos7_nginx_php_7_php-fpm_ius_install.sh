#!/usr/bin/env bash

# Install php7 on nginx (php-fpm)

echo Beginning Install of php7 (php-fpm) from IUS...


# INSTALL PHP7
    echo Installing php7 from IUS...
    sudo yum install -y php70u-cli php70u-common php70u-bcmath php70u-devel php70u-devel php70u-fpm php70u-fpm-nginx php70u-gd php70u-ldap php70u-mbstring php70u-mcrypt php70u-mysql php70u-pdo php70u-pear php70u-process php70u-tidy php70u-xml php70u-pecl-imagick php70u-opcache php70u-mysqlnd php70u-pear php70u-json  > /dev/null
    if [ $? != 0 ]; then exit 1; fi

    echo ... applying configs ...

    # Set cgi.fix_pathinfo=0 in php.ini
    sed -i 's@;cgi.fix_pathinfo=1@cgi.fix_pathinfo=0@' /etc/php.ini > /dev/null
    if [ $? != 0 ]; then exit 1; fi

    # Apply php-fpm config file  (on this server we run php-fpm via socket)
    mv /etc/php-fpm.d/www.conf /etc/php-fpm.d/www.conf.orig  > /dev/null
    curl -o /etc/php-fpm.d/www.conf https://gitlab.com/tsalex/deployment-scripts/raw/master/webserver_files/centos-nginx-conf/etc-php-fpm.d/www.conf > /dev/null
    if [ $? != 0 ]; then exit 1; fi
    echo ...... php7 (php-fpm) installed


# Start & Enable php-fpm Service
    echo Start and Enable php-fpm service...
    systemctl start php-fpm  > /dev/null
    systemctl enable php-fpm > /dev/null
    if [ $? != 0 ]; then exit 1; fi
    echo ...php-fpm service enabled


#Finish Up
echo ........ php7 (php-fpm) install DONE