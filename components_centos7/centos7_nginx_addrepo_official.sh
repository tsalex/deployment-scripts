#!/usr/bin/env bash

# Install MariaDB official repository

# ====== Define Function =======

	# Add MARIADB OFFICIAL REPO
addRepo_nginx(){       
    echo Installing Nginx official repositories...
    cat > /etc/yum.repos.d/nginx.repo <<EOF
[nginx]
name=nginx repo
baseurl=http://nginx.org/packages/mainline/centos/7/\$basearch/
gpgcheck=0
enabled=1
EOF
	echo ...NGINX repo added
}


# ====== Run =======

	# ADD REPO
	addRepo_nginx