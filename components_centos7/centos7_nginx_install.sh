#!/usr/bin/env bash

# Install Nginx
	echo Installing nginx...
    yum install -y nginx > /dev/null
    if [ $? != 0 ]; then exit 1; fi

    # Create HTML directory
    echo ...creating webroot directory ...
    if ! [ -d /var/www ]; then mkdir /var/www; fi
    if ! [ -d /var/www/html ]; then mkdir /var/www/html; fi
    if ! [ -d /var/www/html/public_html ]; then mkdir /var/www/html/public_html; fi
    if [ $? != 0 ]; then exit 1; fi

    # Apply default site config
    echo ... applying config files ...
    mv /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.orig  > /dev/null
    curl -o /etc/nginx/conf.d/default.conf https://gitlab.com/tsalex/deployment-scripts/raw/master/webserver_files/centos-nginx-conf/etc-nginx-conf.d/default.conf > /dev/null

    # Move index files to web root dir
    echo ... moving index file ...
    mv /usr/share/nginx/html/index.html /var/www/html/public_html/index.html > /dev/null
    if [ $? != 0 ]; then exit 1; fi

	# Set permissions
    echo ... setting permissions ...
    chown -R nginx:nginx /var/www/html/public_html > /dev/null
    chmod -R 755 /var/www/html/public_html > /dev/null
    if [ $? != 0 ]; then exit 1; fi

    # Restore SELinux file context for default website root directory
    restorecon -Rv /var/www/html


# Start & Enable Services
    echo Start and Enable nginx service...
    systemctl start nginx > /dev/null
    systemctl enable nginx > /dev/null
    if [ $? != 0 ]; then exit 1; fi
    echo ...nginx service done


# Finish Up
echo ........ nginx DONE