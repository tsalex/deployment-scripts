#!/usr/bin/env bash


# ====== Define Function =======

addRepo_ius(){    
    echo Installing ius repositories...
    curl -sS https://setup.ius.io/ | bash > /dev/null
    # if [ $? != 0 ]; then exit 1; fi
    rpm --import /etc/pki/rpm-gpg/IUS-COMMUNITY-GPG-KEY > /dev/null
	echo ...IUS repo added
}


# ====== Run =======

# ADD REPO
	addRepo_ius