#!/usr/bin/env bash

# Usage:
    # You MUST run this as root!

	# To download:  $curl -O https://gitlab.com/tsalex/deployment-scripts/raw/master/centos7_lemp_php7ius.sh
	# To run remotely:  bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/centos7_lemp_php7ius.sh)
	
	# To run without user prompts...
	# $(curl -sSL https://gitlab.com/tsalex/deployment-scripts/raw/master/centos7_lemp_php7ius.sh | bash)



# Install Software Repositories

    #Add IUS Repo
    bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/components_centos7/centos7_ius_addrepo.sh)

    #Add MariaDB official Repo
    bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/components_centos7/centos7_mariadb_addrepo_official.sh)

    #Add Nginx official Repo
    bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/components_centos7/centos7_nginx_addrepo_official.sh)



# Install Nginx and PHP7 (php-fpm)

    # Install nginx
    bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/components_centos7/centos7_nginx_install.sh)

    # Install php7
    bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/components_centos7/centos7_nginx_php_7_php-fpm_ius_install.sh)

    # Restart nginx service
    systemctl restart nginx > /dev/null



# INSTALL MARIADB
    bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/components_centos7/centos7_mariadb_install.sh)



# Open FIREWALL PORTS for NGINX AND MARIADB

    #Open http and https ports
    bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/components_centos7/centos7_firewall_config_webserver.sh)



# CERTBOT SSL
    # read -p "Would you like to install CertBot SSL? [y/n]? " CERTBOTINSTALL

    # if [[ "$CERTBOTINSTALL" =~ ^([yY][eE][sS]|[yY])+$ ]]
    # then
    #     bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/centos7_lemp_certbotssl.sh)
    # fi


# Copy Basic Nginx sites file


    echo 
    echo 
    echo ... web server components all DONE!
    echo _________________________________________
    echo 
    echo remember to: 
    echo  == 1. run mysql_secure_installation  
    echo  == 2. check if php-fpm works:  /etc/nginx/conf.d  and  /etc/php-fpm.d/www.conf should be configured now   
    echo  == 3. reboot and test
    echo 
    echo  .. NOTE: 
    echo  ...... to install certbot, run this script:
    echo  ...... bash <(curl -s https://gitlab.com/tsalex/deployment-scripts/raw/master/centos7_lemp_certbotssl.sh)
    echo --------------------------------------------------------------
    echo 